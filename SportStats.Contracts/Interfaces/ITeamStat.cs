﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportStats.Contracts.Interfaces
{
    public interface ITeamStat
    {
        ITeam Team {get;set;}
        int Year { get; set; }
    }
}
