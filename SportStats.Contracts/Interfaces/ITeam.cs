﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportStats.Contracts.Interfaces
{
    public interface ITeam
    {
        int TeamID { get; set; }
        string TeamHQ { get; set; }
        string TeamName { get; set; }
        string TeamHtmlLink { get; set; }
    }
}
