﻿CREATE TABLE [dbo].[TeamStatType]
(
	[TeamStatTypeID] INT,
	[TeamStatTypeName] VARCHAR(100) NOT NULL CONSTRAINT [UX_TeamStatTypeName] UNIQUE,
	CONSTRAINT [PK_TeamStatType] PRIMARY KEY CLUSTERED ([TeamStatTypeID])
)
