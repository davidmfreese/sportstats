﻿CREATE TABLE [dbo].[TotalAggType]
(
	[TotalAggTypeId] INT NOT NULL,
	[TotalAggTypeName] VARCHAR(200) NOT NULL CONSTRAINT [UX_TotalAggTypeName] UNIQUE,
	CONSTRAINT [PK_TotalAggTypeId] PRIMARY KEY CLUSTERED ([TotalAggTypeId])
)
