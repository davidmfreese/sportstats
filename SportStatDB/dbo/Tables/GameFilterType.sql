﻿CREATE TABLE [dbo].[GameFilterType]
(
	[GameFilterTypeId] INT NOT NULL,
	[GameFilterTypeName] VARCHAR(200) NOT NULL CONSTRAINT [UX_GameFilterTypeName] UNIQUE,
	CONSTRAINT [PK_GameFilterType] PRIMARY KEY CLUSTERED ([GameFilterTypeId])
)
