﻿CREATE PROCEDURE [dbo].[TeamUpsertTeamStat]
(
	@TeamId int
	, @TeamCity VARCHAR(200)
	, @TeamHtmlLink VARCHAR(4000)
	, @TeamStatTypeId INT
	, @GameFilterTypeId INT
	, @TotalAggTypeId INT
	, @Yr INT
	, @G INT
	, @Min DECIMAL(6,1)
	, @Pts DECIMAL(6,1)
	, @Reb  DECIMAL(6,1)
	, @Ast DECIMAL(6,1)
	, @Stl DECIMAL(6,1)
	, @Blk DECIMAL(6,1)
	, @To DECIMAL(6,1)
	, @Pf DECIMAL(6,1)
	, @Dreb DECIMAL(6,1)
	, @Oreb DECIMAL(6,1)
	, @FGM DECIMAL(6,1)
	, @FGA DECIMAL(6,1)
	, @FGPct DECIMAL(6,1)
	, @3PM DECIMAL(6,1)
	, @3PA DECIMAL(6,1)
	, @3PPct DECIMAL(6,1)
	, @FTM DECIMAL(6,1)
	, @FTA DECIMAL(6,1)
	, @FTPct DECIMAL(6,1)
	, @Eff DECIMAL(6,1)
	, @Deff DECIMAL(6,1)
)
AS
	SET IDENTITY_INSERT dbo.Team ON
	MERGE INTO dbo.Team t
	USING 
	(
		SELECT @TeamId TeamId, @TeamCity TeamCity, @TeamHtmlLink TeamHtmlLink
	) s ON s.TeamId = t.TeamId
	WHEN MATCHED AND 
			(
				(t.TeamCity <> s.TeamCity OR t.TeamCity IS NULL)
				AND (t.TeamHtmlLink <> s.TeamHtmlLink OR t.TeamHtmlLink IS NULL)
			)
	THEN UPDATE
		SET t.TeamCity = s.TeamCity,
			t.TeamHtmlLink = s.TeamHtmlLink,
			t.ModifiedDate = GETDATE()
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(TeamId, TeamCity, TeamHtmlLink)
		VALUES(s.TeamId, s.TeamCity, s.TeamHtmlLink);

	SET IDENTITY_INSERT dbo.Team OFF

	MERGE INTO dbo.TeamStat t
	USING 
	(
		Select 	@TeamId TeamId
				, @TeamStatTypeId TeamStatTypeId
				, @GameFilterTypeId GameFilterTypeId
				, @TotalAggTypeId TotalAggTypeId
				, @Yr Yr
				, @G G
				, @Min 'Min'
				, @Pts Pts
				, @Reb  Reb
				, @Ast Ast
				, @Stl Stl
				, @Blk Blk
				, @To 'To'
				, @Pf Pf
				, @Dreb Dreb
				, @Oreb Oreb
				, @FGM FGM
				, @FGA FGA
				, @FGPct FGPct
				, @3PM '3PM'
				, @3PA '3PA'
				, @3PPct '3PPct'
				, @FTM FTM
				, @FTA FTA
				, @FTPct FTPct
				, @Eff Eff
				, @Deff Deff
	)s 
		ON s.TeamId = t.TeamId AND s.TeamStatTypeId = t.TeamStatTypeId AND s.GameFilterTypeId = t.GameFilterTypeId AND s.Yr = t.Yr
	WHEN NOT MATCHED THEN
		INSERT(TeamId, TeamStatTypeId, GameFilterTypeId, TotalAggTypeId, Yr, G, [Min], Pts, Reb, Ast, Stl, Blk, [To], Pf, Dreb, Oreb, FGM, FGA, FGPct, [3PM], [3PA], FTM, FTA, FTPct, eff, deff)
		VALUES(s.TeamId, s.TeamStatTypeId, s.GameFilterTypeId, s.TotalAggTypeId, s.Yr, s.G, s.[Min], s.Pts, s.Reb, s.Ast, s.Stl, s.Blk, s.[To], s.Pf, s.Dreb, s.Oreb, s.FGM, s.FGA, s.FGPct, s.[3PM], s.[3PA], s.FTM, s.FTA, s.FTPct, s.eff, s.deff);
			

RETURN 0
