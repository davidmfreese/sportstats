﻿using SportStats.Contracts.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SportStats.Core.DataExtraction
{
    public interface ITeamStatExtractor
    {
        IList<ITeamStat> ExtractTeamStats(int year);
    }
}
