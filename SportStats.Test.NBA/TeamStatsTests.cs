﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SportStats.NBA.DataExtraction;
using SportStats.NBA.Models;
using System.Collections.Generic;
using System.Linq;

namespace SportStats.Test.NBA
{
    [TestClass]
    public class TeamStatsTests
    {
        [TestMethod]
        public void TestTeamStats()
        {
            NBATeamStatExtractor teamStats = new NBATeamStatExtractor(false, TeamStatType.Team, GameFilterType.Overall, TotalAggType.Totals);
            //var test = teamStats.ExtractTeamStats(14);
        }

        [TestMethod]
        public void TestAllTeamStatsForYear()
        {
            List<string> allRows = new List<string>();
            foreach (var t in Enum.GetNames(typeof(TeamStatType)))
            {
                TeamStatType teamStatType = (TeamStatType)Enum.Parse(typeof(TeamStatType), t, true);
                if (teamStatType == TeamStatType.None)
                {
                    continue;
                }

                foreach (var g in Enum.GetNames(typeof(GameFilterType)))
                {
                    GameFilterType gameFilterType = (GameFilterType)Enum.Parse(typeof(GameFilterType), g);
                    if (gameFilterType == GameFilterType.None)
                    {
                        continue;
                    }

                    foreach (var a in Enum.GetNames(typeof(TotalAggType)))
                    {
                        TotalAggType totalAggType = (TotalAggType)Enum.Parse(typeof(TotalAggType), a);

                        NBATeamStatExtractor teamStats = new NBATeamStatExtractor(false, teamStatType, gameFilterType, totalAggType);
                        var rows = teamStats.ExtractTeamStatsCSV(14);
                        if (rows != null && rows.Count > 0)
                        {
                            allRows.AddRange(rows);
                        }
                    }
                }
            }

            foreach (var row in allRows)
            {
                Console.WriteLine(row);
            }
        }

        [TestMethod]
        public void TestAllTeamStatsForYearCrossApply()
        {
            
            var teamStatTypes = Enum.GetNames(typeof(TeamStatType)).Select(t => (TeamStatType)Enum.Parse(typeof(TeamStatType), t, true)).Where(t=>t != TeamStatType.None);
            var gameFilterTypes = Enum.GetNames(typeof(GameFilterType)).Select(t => (GameFilterType)Enum.Parse(typeof(GameFilterType), t, true)).Where(t => t != GameFilterType.None);
            var totalAggTypes = Enum.GetNames(typeof(TotalAggType)).Select(t => (TotalAggType)Enum.Parse(typeof(TotalAggType), t, true)).Where(t => t != TotalAggType.None);

            var crossproduct = from t in teamStatTypes
                               from g in gameFilterTypes
                               from a in totalAggTypes
                               select new { teamStatType = t, gameFilterType = g, totalAggType = a };

            
            List<string> allRows = new List<string>();
            foreach (var group in crossproduct)
            {
                NBATeamStatExtractor teamStats = new NBATeamStatExtractor(false, group.teamStatType, group.gameFilterType, group.totalAggType);
                var rows = teamStats.ExtractTeamStatsCSV(14);
                if (rows != null && rows.Count > 0)
                {
                    allRows.AddRange(rows);
                }
            }

            foreach (var row in allRows)
            {
                Console.WriteLine(row);
            }
        }
    }
}
