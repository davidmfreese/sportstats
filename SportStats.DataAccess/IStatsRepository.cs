﻿using SportStats.Contracts.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportStats.DataAccess
{
    public interface IStatsRepository
    {
        bool SaveTeamStat(ITeamStat Stat);
    }
}
