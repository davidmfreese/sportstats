﻿using HtmlAgilityPack;
using SportStats.Core.DataExtraction;
using SportStats.NBA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fizzler.Systems.HtmlAgilityPack;
using Fizzler;
using Fizzler.Systems;

namespace SportStats.NBA.DataExtraction
{
    public class NBATeamStatExtractor : ITeamStatExtractor
    {
        //nba/teamstats/{YY}/{TeamStatType}/diffeff/{gamefiltertype}-{totalAggType}
        public static string HOOPSTATS_COM_BASE_URL = @"http://www.hoopsstats.com/basketball/fantasy/nba/{0}/{1}/{2}/diffeff/{3}-{4}";
        public static string TEAM_STATS_URLPLUG = "teamstats";
        public static string TEAM_OPPONENT_STATS_URLPLUG = "opponentstats";

        public readonly TeamStatType TeamStatType;
        public readonly GameFilterType GameFilterType;
        public readonly TotalAggType TotalAggType;
        public readonly bool OpponentStats;

        public NBATeamStatExtractor(bool getOpponentStats, TeamStatType teamStatType, GameFilterType gameFilterType, TotalAggType totalAggType)
        {
            this.OpponentStats = getOpponentStats;
            this.TeamStatType = teamStatType;
            this.GameFilterType = gameFilterType;
            this.TotalAggType = totalAggType;
        }

        public IList<Contracts.Interfaces.ITeamStat> ExtractTeamStats(int year)
        {
            throw new NotImplementedException();
        }

        public IList<string> ExtractTeamStatsCSV(int year)
        {
            IList<Contracts.Interfaces.ITeamStat> teamStats = new List<Contracts.Interfaces.ITeamStat>();
            string stattype = this.OpponentStats ? TEAM_OPPONENT_STATS_URLPLUG : TEAM_STATS_URLPLUG;
            string endpoint = String.Format(HOOPSTATS_COM_BASE_URL, stattype, year, (int)TeamStatType, (int)GameFilterType, (int)TotalAggType);

            var getHtmlWeb = new HtmlWeb();
            var doc = getHtmlWeb.Load(endpoint).DocumentNode;

            var rows = doc.QuerySelectorAll("table.statscontent tr");// tbody tr
            List<string> csvRows = new List<string>();
            //TODO: Need to link each row with the Keys (TeamStatType teamStatType, GameFilterType gameFilterType, TotalAggType totalAggType)s
            foreach (var row in rows) 
            {
                string csv = "";
                var cells = row.ChildNodes.Where(x=>x.InnerText != null && x.InnerText.Trim().Length > 0).ToList();
                if (cells != null && cells.Count() == 21)
                {
                    csv += cells[0].InnerText;
                    for (int i = 1; i < cells.Count(); i++)
                    {
                        csv += "," + cells[i].InnerText.Replace("-", ",");
                    }
                }
                
                Console.WriteLine(csv);
                csvRows.Add(csv);
            }

            return csvRows;
        }
    }
}
