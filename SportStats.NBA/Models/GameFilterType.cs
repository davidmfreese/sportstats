﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SportStats.NBA.Models
{
    public enum GameFilterType
    {
        None = 0,
        Overall,
        Home,
        Road,
        Last3Games,
        Last5Games,
        Last10Games,
        RegularSeason,
        Playoffs,
        VsPlayoffTeams,
        VsLotteryTeams,
        VsEastTop8,
        VsWestTop8,
        VsPlus500Teams,
        VsSub500Teams,
        VsEast,
        VsWest,
        PreAllstar,
        PostAllStar,
        Over100Pts,
        Under100Pts,
        GamesWon,
        GamesLost,
        October,
        November,
        December,
        January,
        February,
        March,
        April
    }
}
