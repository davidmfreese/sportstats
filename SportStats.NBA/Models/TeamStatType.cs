﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SportStats.NBA.Models
{
    public enum TeamStatType
    {
        None=0,
        Team=1,
        Backcourt=2,
        Frontcourt=3,
        InThePaint=4,
        OutOfPaint=5,
        Starters=6,
        Bench=7,
        SFPostiion=8,
        PFPosition=9,
        CPosition=10,
        SMPosition=11,
        PGPosition=12
    }
}
