﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SportStats.NBA.Models
{
    public enum TotalAggType
    {
        None = 0,
        Totals = 1,
        Averages = 2,
        Per48Minutes = 3
    }
}
